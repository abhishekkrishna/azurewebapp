
// ------------- Starts running the bot by setting up a websocket connection -------------


// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------


/*
////////////////////////////////////////////////////////////////////
        THIS SECTION CONTAINS THE LIST OF REQUIRED PACKAGES
////////////////////////////////////////////////////////////////////
*/


// Request is used for making HTTP calls
// More details about Request can be found at https://www.npmjs.com/package/request
var request = require("request");

// azure-storage provides a Node.js package and a browser compatible JavaScript Client Library 
// that make it easy to consume and manage Microsoft Azure Storage Services.
// More details about azure-storage can be found at https://www.npmjs.com/package/azure-storage
var azure = require("azure-storage");

// botConnect is one of our files and handles almost all the bot related functionality
var botConnect = require("./botConnect.js");


//------------------------------------------------------------------------------------------------------------

module.exports = 
{
    Bot: function()
    {
        "use strict";
        console.log("botStart.js : 1 : Bot function! \n");

        /*
        ////////////////////////////////
        // Start a bot for every team //
        ////////////////////////////////
        */

        // Initialize Azure Table Service
        var tableSvc = azure.createTableService();

        // Query to get all installed teams
        var query = new azure.TableQuery().where('PartitionKey eq ?', "Team");

        tableSvc.queryEntities('SlackDetails', query, null, function (error, result, response) 
        {
            if (!error) 
            {
                // success
                console.log("botStart.js : 2 : Number of teams: " + result.entries.length + "\n");

                // Loop to iterate through all the teams
                for (var i=0; i < result.entries.length; i++)
                {
                    var AccessToken = result.entries[i].AccessToken['_'];
                    var BotAccessToken = result.entries[i].BotAccessToken['_'];

                    // Call the botConnect function in botConnect.js
                    botConnect.botConnect(AccessToken, BotAccessToken);
                }
            }
            
            else 
            {
                // Error
                console.log("botStart.js : 3 : ERROR! Bot could not be started! \n");
            }
        });
    }
};
