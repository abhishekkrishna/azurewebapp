// Send object

var request = require("request");
var analytics = require("./analytics.js");
module.exports = {
    SendObject: function (SlackID,responseURL, JSONmsg,flag) {
        "use strict";
        request({
            url: responseURL,
            method: "POST",
            json: JSONmsg,
            headers: {
                "content-type": "application/json",
            }
        }, function(error, response, body) {
            if (!error) {
                console.log("Successfully sent object");
                console.log(flag);
                if(flag){
                    console.log("entered if loop");
                    var attachmentEvent = {
                        "user_id" : SlackID,
                        "event_type" : "PostInChannel",
                    };
                    analytics.SendData(attachmentEvent);
                }
            } else {
                console.log("Error sending object");
            }
        });
    }
}