// Search through the user's highlights and return the most relevant posts
// PARAMETERS: MediumUserID, Keyword, responseURL

// require this stuff
var azure = require("azure-storage");
var slashResponse = require("./slashResponse");

module.exports = {
    Search: function (SlackUserID, Keyword, responseURL, pageNumber) {
        // Initialize Azure Table Service
        var tableSvc = azure.createTableService();

        tableSvc.retrieveEntity('MediumUsers', 'User', SlackUserID, function (error, result, response) {
            if (!error) {
                // Successful
                var MediumUserID = result.MediumUserID['_'];

                // Query to get user highlights from table
                var query = new azure.TableQuery().where('PartitionKey eq ?', MediumUserID);

                tableSvc.queryEntities('MediumHighlights', query, null, function (error, result, response) {
                    if (!error) {
                        // query was successful

                        console.log("Number of search results retrieved: " + result.entries.length);

                        // Analyse and find 5 posts most relevant to the keyword
                        var relevanceScoreArray = [];
                        var highlightObjects = result.entries;

                        for (var i=0; i<highlightObjects.length; i++) {
                            var score = 1;

                            score += countOcurrences(highlightObjects[i].Paragraph['_'], Keyword);
                            score += countOcurrences(highlightObjects[i].PostName['_'], Keyword);
                            score += countOcurrences(highlightObjects[i].PostAuthor['_'], Keyword);

                            relevanceScoreObject = {
                                "index": i,
                                "score": score,
                                "time": highlightObjects[i].TimeCreated['_']
                            }

                            relevanceScoreArray.push(relevanceScoreObject);
                        }

                        // Sorting code
                        // https://stackoverflow.com/a/979289/5640715

                        // First sort the highlights by recency
                        relevanceScoreArray.sort(function (a, b) {
                            return parseInt(b.time) - parseInt(a.time);
                        });

                        // If search term was empty, don't sort by score
                        if (Keyword === "~~~```~~~") {  // Wierd string represents no search term
                            console.log("Search: Empty keyword!");
                        } else {
                            // If a keyword exists, sort by the score

                            // Sorts the array in descending order according to score
                            relevanceScoreArray.sort(function (a, b) {
                                return parseInt(b.score) - parseInt(a.score);
                            });
                        }

                        // Return upto top 5 results as a JSON object
                        returnObj = {"data": []};

                        // Calculate starting and ending value of iterator for the loop
                        var iteratorStart = 5 * (parseInt(pageNumber) - 1);
                        var iteratorEnd = Math.min(iteratorStart + 5, highlightObjects.length); // Can't go beyond number of highlights

                        for (var i = iteratorStart; i < iteratorEnd; i++) {
                            obj = {
                                "postName": highlightObjects[relevanceScoreArray[i].index].PostName['_'],
                                "postAuthor": highlightObjects[relevanceScoreArray[i].index].PostAuthor['_'],
                                "paragraph": highlightObjects[relevanceScoreArray[i].index].Paragraph['_'],
                                "startOffset": highlightObjects[relevanceScoreArray[i].index].StartOffset['_'],
                                "endOffset": highlightObjects[relevanceScoreArray[i].index].EndOffset['_'],
                                "highlightID": highlightObjects[relevanceScoreArray[i].index].RowKey['_']
                            };
                            returnObj.data.push(obj);
                        }

                        // Determine new page number
                        var newPageNumber;
                        // Set it to 0 if no more posts exist
                        if (highlightObjects.length <= parseInt(pageNumber) * 5) {
                            newPageNumber = 0;
                        } else {
                            // Increment the pageNumber
                            newPageNumber = parseInt(pageNumber) + 1;
                        }

                        slashResponse.SlashResponse(SlackUserID,returnObj, responseURL, pageNumber, newPageNumber, Keyword);

                    } else {
                        console.log(error);
                    }
                });
                
                

            } else {
                console.log(error);
            }
        });
        // https://stackoverflow.com/a/15891749/5640715
        function countOcurrences(str, key) {
            var regExp = new RegExp(key, "gi"); // Global search, case-insensitive
            return (str.match(regExp) || []).length;
        }
    }
};

