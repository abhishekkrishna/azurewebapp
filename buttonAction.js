
// Does the most important bit: Responding properly when the user clicks one of the buttons

// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------

var azure = require("azure-storage");
var sendObject = require("./sendObject.js");
var search = require("./search.js");

// --------------------------------------------------------------------------------------------------------------------

module.exports = 
{
    ButtonAction: function (req, res) 
    {
        "use strict";
        res.status(200).end(); // best practice to respond with 200 status
        var actionJSONPayload = JSON.parse(req.body.payload); // parse URL-encoded payload JSON string

        console.log("buttonAction.js : 1 : " + JSON.stringify(actionJSONPayload));

        var slackID = actionJSONPayload.user.id;
        // Check the type of button and do the appropriate thing
        if (actionJSONPayload.actions[0].name == "more") 
        {
            // Call search and return more values
            search.Search(actionJSONPayload.user.id, actionJSONPayload.callback_id, actionJSONPayload.response_url, actionJSONPayload.actions[0].value);
        } 
        
        else if (actionJSONPayload.actions[0].name == "cancel") 
        {
            var responseObj = 
            {
                "response_type": "ephemeral",
                "text": "",
                "delete_original": true
            };
            sendObject.SendObject(slackID,actionJSONPayload.response_url, responseObj, 0);
        } 
        
        else 
        {
            // Get the parameters we need from the Slack request
            var highlightID = actionJSONPayload.actions[0].name;
            //var slackID = actionJSONPayload.user.id;
            var slackName = actionJSONPayload.user.name;

            console.log("buttonAction.js : 2 : " + highlightID);

            // Get all required highlight details from table
            var tableSvc = azure.createTableService();
            // Get medium userID
            tableSvc.retrieveEntity('MediumUsers', 'User', actionJSONPayload.user.id, function (error, result, response) 
            {
                if (!error) 
                {
                    // success
                    var MediumUserID = result.MediumUserID['_'];

                    console.log("buttonAction.js : 3 : " + MediumUserID);

                    // Now get the required highlight
                    tableSvc.retrieveEntity('MediumHighlights', MediumUserID, highlightID, function (error, result, response) 
                    {
                        if (!error) 
                        {
                            // sucess
                            // Get required values from the table result
                            var startOffset = result.StartOffset['_'];
                            var endOffset = result.EndOffset['_'];
                            var paragraph = result.Paragraph['_'];

                            // Extract the quote from the paragraph
                            var quote = paragraph.substring(startOffset, endOffset);

                            // Delete the original message
                            var responseObj = 
                            {
                                "response_type": "ephemeral",
                                "text": "",
                                "delete_original": true
                            };

                            // Send a new in_channel message with the selected highlight
                            var finalHighlightObj = 
                            {
                                "response_type": "in_channel",
                                "text": "",
                                "replace_original": false,
                                "attachments": 
                                [
                                    {
                                        "pretext": "Here's a highlight shared by *<@" + slackID + "|" + slackName + ">*",
                                        "title": result.PostName['_'],
                                        "title_link": result.PostURL['_'],
                                        "text": "_*" + quote + "*_",
                                        "footer_icon": result.ImageURL['_'],
                                        "footer": result.PostAuthor['_'],
                                        "color": "#0ce071",
                                        "fallback": quote,
                                        "mrkdwn_in": [
                                            "text",
                                            "pretext"
                                        ]
                                    }
                                ]
                            };

                            sendObject.SendObject(slackID,actionJSONPayload.response_url, finalHighlightObj, 1);
                            sendObject.SendObject(slackID,actionJSONPayload.response_url, responseObj, 0);

                        } 
                        
                        else 
                        {
                            console.log("buttonAction.js : 4 : " + error);
                        }
                    });
                } 
                
                else 
                {
                    console.log("buttonAction.js : 5 : " + error);
                }
            });
        }
    }
};