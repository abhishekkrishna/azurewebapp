// Send welcome message
// Sends messages to Slack using the RTM API

"use strict";

var WebSocket = require("ws");
var request = require("request");
var azure = require("azure-storage");

module.exports = {
    send: function(TeamID, welcomeArr) {

        // Initialize Azure Table Service
        var tableSvc = azure.createTableService();

        tableSvc.retrieveEntity('SlackDetails', 'Team', TeamID, function (error, result, response){
            if(!error){
                // result contains the entity

                // First need to make a POST request
                var URL = "https://slack.com/api/rtm.connect";
                var TOKEN =  result.BotAccessToken['_'];

                    request.post (URL + '?token=' + TOKEN, function (request, response, body) {
                        var Obj = JSON.parse(body);
                        //console.log(Obj.ok);
                        if (Obj.ok) {
                            // Set-up websocket
                            var ws = new WebSocket(Obj.url);
                            ws.on('open', data => {

                                for (var i=0; i<welcomeArr.length; i++) {

                                    //setTimeout(function() {
                                        ws.send(JSON.stringify(welcomeArr[i]));
                                        console.log("Sent message to: " + welcomeArr[i].channel);
                                    //}, 500);

                                }
                            });

                            ws.on('close', data => {
                                //console.log("SendMessage: WebSocket closed");
                            });
                        } else {
                            console.log("SendMessage Error 1: " + response.body);
                        }
                    });

            } else {
                console.log("SendMessage Error 2: " + response.body);
            }
        });

    }
}