// Sets up a websockets connection and keep listening
// Also handles all bot responses

// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------

var request = require("request");
var WebSocket = require("ws");
var azure = require("azure-storage");
var analytics = require("./analytics")
var initialSetup = require("./initialSetup");
var help = require("./help.js");
var sendWelcomeMessage = require("./sendWelcomeMessage.js");

// -------------------------------------------------------------------------------------------------------------------


module.exports = 
{
    botConnect: function(AccessToken, BotAccessToken) 
    {
        "use strict";

        // URL for connecting to Slack with the Real Time Messaging API
        var URL = "https://slack.com/api/rtm.connect";

        console.log("botConnect.js : 1 : Bot connection access token: " + BotAccessToken);

        request.post (URL + '?token=' + BotAccessToken, function (req, response, body) 
        { 
            //request changed to req
            var Obj = JSON.parse(body);

            if (Obj.ok) 
            {
                console.log("botConnect.js : 2: Bot ID: " + Obj.self.id);
                StartWebsockets(Obj.url, Obj.self.id, Obj.team.id);
            } 
            
            else 
            {
                //console.log(response);  // error
            }
        });
    }
};


function StartWebsockets (URL, BotUserID, TeamID) 
{
    "use strict";

    var ws = new WebSocket(URL);
    ws.on('open', data => 
    {
        console.log("botConnect.js : 3: Connection set-up.");

        var pingObj = 
        {
            "id": 1, // Random value
            "type": "ping",
        };

        // Keep pinging at regular intervals to keep the connection alive
        setInterval(function()
        {
            ws.send(JSON.stringify(pingObj));
            console.log("botConnect.js : 4: Ping.");

        }, 8000);
    });

    ws.on('message', data => 
    {
        var dataObj=JSON.parse(data);
        console.log("botConnect.js : 5 : " + dataObj);

        // Only process message events and those not sent by the bot itself
        if(dataObj.type === 'message' && dataObj.user != BotUserID) 
        {
            if (dataObj.channel == 'C6C64KRPT') 
            {
                // Don't respond in feedback channel
                return;
            }

            var channel = dataObj.channel;
            var slackUserID = dataObj.user;

            /*
                Analyze the message and take suitable action
            */

            // Prevent error while splitting empty string
            if (dataObj.text == undefined) 
            {
                return;
            }
            var messageWords = dataObj.text.split(' ');

            /*
            ///////////////////////////////////////////////
                            CHECK FOR HELP 
            ///////////////////////////////////////////////
            */

            if (messageWords[0].toUpperCase() == "HELP") 
            {
                console.log("botConnect.js : 6 : Help called");
                var responseObj = 
                {
                        "user": slackUserID,
                        "type": "message",
                        "channel": channel,
                        "text": "",
                        "mrkdwn_in": [
                            "text"
                        ]
                };

                help.help(responseObj, TeamID);
                var helpEvent = 
                {
                    "user_id" : slackUserID,
                    "event_type" : "helpCalled"
                };
                analytics.SendData(helpEvent);
            }

            /*
            ///////////////////////////////////////////////
                            END OF HELP 
            //////////////////////////////////////////////
            */
            
            //-----------------------------------------------------------------------------------------

            /*
            /////////////////////////////////////////////
                       USERNAME UPDATE BEGINS 
            ////////////////////////////////////////////
            */

            else if (messageWords[0].toUpperCase() == "USER" || messageWords[0].toUpperCase() == "USERNAME") 
            {
                console.log("botConnect.js : 7 : User update!");
                /*
                Expected format for registering user
                    user <medium username> or username <medium username>
                */
                var responseObj = 
                {
                    "user": slackUserID,
                    "type": "message",
                    "channel": channel,
                    "text": "_Hold on while we update your username..._",
                    "mrkdwn_in": 
                    [
                        "text"
                    ]
                };
                ws.send(JSON.stringify(responseObj));
                // Register the username
                initialSetup.Setup(slackUserID, messageWords[1], responseObj, TeamID);

            }
            
            /*
            ///////////////////////////////////////////////
                       END OF USERNAME UPDATE 
            //////////////////////////////////////////////

            //////////////////////////////////////////////
                        CHECK FOR RANDOM MSG 
            /////////////////////////////////////////////
            */

            else 
            {
                console.log("botConnect.js : 8 : Some other message");
                console.log("botConnect.js : 9 : " + channel + ' ' + slackUserID);

                // UserID has to exist and be a string
                if (typeof slackUserID != "string") 
                {
                    return;
                }

                // Check if the user already has setup the app
                var tableSvc = azure.createTableService();

                // error checking (no clue why this causes an exception some times)
                var userString = dataObj.user;
                if (typeof dataObj.user != "string") 
                {
                    userString = dataObj.user.toString();
                }

                tableSvc.retrieveEntity('MediumUsers', 'User', userString, function(error, result, response) 
                {
                    if(!error)
                    {
                        // the username already exists in our table and has been setup
                        var responseObj = 
                        {
                            "user": slackUserID,
                            "type": "message",
                            "channel": channel,
                            "text": "*You can access the Medium highlights of `" + result.MediumName['_'] + "` using the slash command!*\n Type `help` if you need more information!\n",
                            "mrkdwn_in": 
                            [
                                "text"
                            ]
                        };
                        ws.send(JSON.stringify(responseObj));
                    } 
                    
                    else 
                    {
                        // Request user to add their username
                        var responseObj = {
                            "user": slackUserID,
                            "type": "message",
                            "channel": channel,
                            "text": "Enter a Medium username to get highlights. To do this, type 'user' followed by your Medium username."
                        };
                        ws.send(JSON.stringify(responseObj));
                    }
                });
            }
            // End of message analysis
        }
    });

    ws.on('close', data => 
    {
        console.log("botConnect.js : 10 : console closed");
    });
}