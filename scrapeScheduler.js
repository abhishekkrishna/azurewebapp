// Automates the scraping so that new highlights on Medium also get added to our database



// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------


/*
////////////////////////////////////////////////////////////////////
        THIS SECTION CONTAINS THE LIST OF REQUIRED PACKAGES
////////////////////////////////////////////////////////////////////
*/


// azure-storage provides a Node.js package and a browser compatible JavaScript Client Library 
// that make it easy to consume and manage Microsoft Azure Storage Services.
// More details about azure-storage can be found at https://www.npmjs.com/package/azure-storage
var azure = require("azure-storage");

// scraper.js is one of our own files that retrieves highlights from the Medium Profile of the User
var scraper = require("./scraper.js");


//---------------------------------------------------------------------------------------------------------------------

module.exports = 
{
    automate: function () 
    {
        "use strict";
        setInterval(runScraper, 1000 * 60 * 10); // Schedule scraper to run every 10 minutes
    }
};

function runScraper() 
{
    // Initialize Azure Table Service
    var tableSvc = azure.createTableService();
    // Query to get all installed users
    var query = new azure.TableQuery().where('PartitionKey eq ?', "User");

    tableSvc.queryEntities('MediumUsers', query, null, function (error, result, response) 
    {
        if (!error) 
        {
            // success
            console.log("scrapeScheduler.js : 1 : Number of users: " + result.entries.length);

            // Loop to iterate through all the users
            for (var i=0; i < result.entries.length; i++)
            {
                var SlackUserID = result.entries[i].RowKey['_'];
                var MediumUserID = result.entries[i].MediumUserID['_'];

                scraper.Scraper(SlackUserID, MediumUserID, {}, 'Random Name', 0, 0); //added by giri @ 1524 hrs on 28/7
            }
        } 
        
        else 
        {
            console.log("scrapeScheduler.js : 2 : Not start error!");
        }
    });
}

