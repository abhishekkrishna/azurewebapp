﻿/*
////////////////////////////////////////////////////////
///// VERSION 0.1, REVERT TO THIS IN CASE OF ISSUES/////
////////////////////////////////////////////////////////
*/



// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------

// Use strict mode
"use strict";

/*
////////////////////////////////////////////////////////////////////
        THIS SECTION CONTAINS THE LIST OF REQUIRED PACKAGES
////////////////////////////////////////////////////////////////////
*/


// Express JS is a web framework for Node
// More details about Express can be found at https://www.npmjs.com/package/express
var express = require('express');
var app = express();

// Tell Web Server to either listen on PORT 1337 or on whatever is in the environment variable PORT
var port = process.env.port || 1337; 

// JSDOM is used for testing and scraping web applications
// More details about JSOM can be found at https://www.npmjs.com/package/jsdom
const jsdom = require("jsdom");
const {JSDOM} = jsdom;

// Request is used for making HTTP calls
// More details about Request can be found at https://www.npmjs.com/package/request
var request = require("request");

// jsonQ is used for manipulating JSON objects, which can be found in plenty throughout the code
// More details about Request can be found at https://www.npmjs.com/package/jsonq
var jsonq = require("jsonq");

// body-parser is a Node.js body parsing middleware 
// More details about Request can be found at https://www.npmjs.com/package/body-parser
var bodyParser = require("body-parser");
var urlencodedParser = bodyParser.urlencoded({extended: true});

// node-php is used to run php scripts inside the Express site. Required for sending e-mails from the contact form
// More details about Request can be found at https://www.npmjs.com/package/node-php
var php = require('node-php');

// The path module provides utilities for working with file and directory paths. 
// More details about Request can be found at https://nodejs.org/docs/latest/api/path.html
var path = require('path');

//------------------------------------------------------------------------------------------------------------

/*
/////////////////////////////////////////////////////////////
                    SETTING UP MIDDLEWARE 
////////////////////////////////////////////////////////////
*/

app.use(urlencodedParser);
app.use(bodyParser.json());
app.use(express.static(__dirname + '/web'));

/*
///////////////////////////////////////////////////////////
                   END OF MIDDLEWARE SETUP 
///////////////////////////////////////////////////////////
*/

//------------------------------------------------------------------------------------------------------------


// Start listening to requests
var server = app.listen(port, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Server.js : 1 : Example app listening at http://%s:%s', host, port);

    // Start the bot medium_highlights
    console.log("Server.js : 2 : Starting bot automatically...");
    var bot = require("./botStart.js");
    bot.Bot();

    // Start Scrape Scheduler, which regularly re-scrapes and updates every user's highlights
    var scrapeScheduler = require("./scrapeScheduler.js");
    scrapeScheduler.automate();
});

//---------------------------------------------------------------------------------------------------------------------------

// Homepage
app.get('/', function (req, res) {
    res.sendFile('index.html');
});

//---------------------------------------------------------------------------------------------------------------------------

// App install success page
app.get('/success', function (req, res) {
    res.sendFile('success.html', { root: path.join(__dirname, '/web') });
});

//---------------------------------------------------------------------------------------------------------------------------

// App install error page
app.get('/error', function(req, res) {
    res.sendFile('error.html', { root: path.join(__dirname, '/web') });
});

//---------------------------------------------------------------------------------------------------------------------------

// Handles the contact form on home page
app.post("/contact", function(req, res) {
    var contact = require("./contact.js");
    contact.send(req, res);
});

//---------------------------------------------------------------------------------------------------------------------------

// Takes care of App Authorization, inserts the Team into the Database, Initializes the Bot, and sends the Welcome Message
app.get('/oauth', function(req, res) {
    var oauth = require("./oauth.js");
    oauth.Authorize(req, res);
});

//---------------------------------------------------------------------------------------------------------------------------

// Point of entry when the slash command is invoked
app.post('/slash', function (req, res) {
    var slash = require("./slash.js");
    slash.Slash(req, res);
});

//---------------------------------------------------------------------------------------------------------------------------

// Triggered when a button is pressed in the initial response to the user after the slash command is invoked
app.post('/actions', urlencodedParser, function (req, res){
    var buttonAction = require("./buttonAction.js");
    buttonAction.ButtonAction(req, res);
});

//---------------------------------------------------------------------------------------------------------------------------

/*
///////////////////////////////////////////////////////////
/////////////////// END OF FILE //////////////////////////
//////////////////////////////////////////////////////////
*/