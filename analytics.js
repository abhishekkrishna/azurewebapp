// Does some cool analytics stuff

// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------

var request = require("request");

//--------------------------------------------------------------------------------------------------------------------


module.exports = 
{
    SendData: function(event) 
    {
        var api_key  = "96293735665fc2ebc30d9767c13acfb5";
        // var event = '[{"user_id":"john_doe@gmail.com", "event_type":"watch_tutorial", "user_properties":{"Cohort":"Test A"}, "country":"United States", "ip":"127.0.0.1", "time":1396381378123}]';
        var eventString = JSON.stringify(event);
        request.post("https://api.amplitude.com/httpapi?api_key=" + api_key + "&event=" + eventString, function(error, response, body) 
        {
            console.log("analytics.js : 1 : " + response.body);
            console.log("analytics.js : 2 : event data sent");
        });
    }
}

// -------------------------------------------------------------------------------------------------------------------