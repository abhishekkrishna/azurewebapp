// Bot sends a message to help the user by providing instructions

// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------

module.exports = 
{
    help: function (responseObj, TeamID) 
    {
        "use strict";
        var sendMessage = require("./sendMessage.js");

        var helptext = "\n\n*Help is here!*\n\n" +
                "_*This app lets you retrieve your highlights from Medium (medium.com) and send them as messages to your Slack team!*_\n" +
                "_*Here's a list of things you can do:*_ \n\n" +
                ">1. Send me a message of the form `user <Medium Username>` to tell us which Medium account to retrieve highlights from. If you don't know your medium username, you can find it in the URL of your profile page at medium.com.\n" +
                ">2. Type `/medium` to get your 5 most recent highlights\n" +
                ">3. Type `/medium <Keyword>` to get 5 highlights that we think are most relevant to your keyword.\n";

        responseObj.text = helptext;
        sendMessage.send(responseObj, TeamID, 0);
    }
};