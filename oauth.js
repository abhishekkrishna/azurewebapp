
// --------------------------- Authenticates teams and enables installation of the app ------------------------------


// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------

/*
////////////////////////////////////////////////////////////////////
        THIS SECTION CONTAINS THE LIST OF REQUIRED PACKAGES
////////////////////////////////////////////////////////////////////
*/

var request = require("request");
var azure = require('azure-storage');
var analytics = require('./analytics.js')

var botConnect = require("./botConnect");
var sendWelcomeMessage = require("./sendWelcomeMessage.js");

// ------------------------------------------------------------------------------------------------------------------

module.exports = 
{
    Authorize: function (req, res)
    {

        // Check if permission was denied / error occurred
        if (req.query.error) 
        {
            res.redirect("/error");
        }

        ///////////////////////////////////////////////
        // Now proceed with normal installation flow //
        ///////////////////////////////////////////////

        // Slack returns a code
        console.log("oauth.js : 1 : Code: " + req.query.code);

        // Use the oauth.access method to get tokens
        var URL = "https://slack.com/api/oauth.access";
        var CLIENT_ID = "201958636709.204830410167";
        var CLIENT_SECRET = "6b87df0f20f48c4396597be6c8b655cf";

        URL += "?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&code=" + req.query.code;

        request(URL, function (error, response, body)
        {
            // Slack returns the deatils of the team and access tokens

            console.log("oauth.js : 2 : Request");
            console.log("oauth.js : 3 : " + body);

            var jsonObj = JSON.parse(body);

            console.log("\n\noauth.js : 4 : " + jsonObj.team_name + "\n");
            console.log("oauth.js : 5 : " + jsonObj.bot + "\n");
            console.log("oauth.js : 6 : " + jsonObj.bot.bot_user_id + "\n");

            // Create a JSON object
            var TeamObj = 
            {
                PartitionKey: {'_': "Team"},
                RowKey: {'_': jsonObj.team_id},
                AccessToken: {'_': jsonObj.access_token},
                Scope: {'_': jsonObj.scope},
                TeamName: {'_': jsonObj.team_name},
                BotUserID: {'_': jsonObj.bot.bot_user_id},
                BotAccessToken: {'_': jsonObj.bot.bot_access_token}
            };


            // Initialize Azure Table Service
            var tableSvc = azure.createTableService();

            tableSvc.insertOrReplaceEntity('SlackDetails', TeamObj, function (error, result, response)
            {
                if (!error) 
                {
                    console.log("oauth.js : 7 : Inserted team " + jsonObj.team_name + " with ID " + jsonObj.team_id);
                    botConnect.botConnect(jsonObj.access_token, jsonObj.bot.bot_access_token);

                    // Send welcome message to all users in this team
                    request("https://slack.com/api/im.list?token=" + jsonObj.bot.bot_access_token, function(error, response, body)
                    {
                        var IMobj = JSON.parse(body);

                        if (IMobj.ok == true)
                        {
                            var imArray = IMobj.ims;
                            var welcomeArray = [];

                            for (var i = 0; i < imArray.length; i++ )
                            {
                                var welcomeObj = 
                                {
                                    "type": "message",
                                    "channel": imArray[i].id,
                                    "text": "The *Medium Highlights* app was just installed to your team!\n"
                                            + "This app lets you retrieve your highlights from Medium (medium.com) and send them as messages to your Slack team!\n"
                                            + "\nI am MediumBot and I can help you set-up the app. You can start by typing `help` to get a better idea of what you can and cannot do.\n",
                                }
                                welcomeArray.push(welcomeObj);
                            }
                            sendWelcomeMessage.send(jsonObj.team_id, welcomeArray);
                        }
                    });

                    // ANALYTICS
                    var installEvent = 
                    {
                        "user_id" : jsonObj.team_id,
                        "event_type" : "appInstalled",
                    };
                    analytics.SendData(installEvent);
                    res.redirect("/success");

                } 
                
                else
                {
                    console.log("oauth.js : 8 : " + response);
                    res.redirect("/error");
                }

            });


        });

    }
};