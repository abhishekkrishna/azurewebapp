// Slash command

var analytics = require('./analytics.js');

// Our functions
var search = require("./search.js");

module.exports = {
    Slash: function (req, res) {
        var reqBody = req.body;
        var keyword = reqBody.text;
        if (reqBody.token != 'en4O0pLksumht6WRxvw95Z93') {
            res.status(403).end("Access forbidden!!");
        } else {
            var slackUserID = reqBody.user_id;
            var responseURL = reqBody.response_url;


            // Check the search term
            if (reqBody.text === "") {
                // Codes to indicate empty search term; We need to show most recent highlights
                keyword = "~~~```~~~";  // Just a placeholder for no keyword
                console.log("Empty keyword");
            }

            // Now call search
            searchEvent={
                    "event_type" : "Search",
                    "user_id" : reqBody.user_id,
                    "event_properties" : {"searchTerm" : keyword}
                };
            search.Search(slackUserID, keyword, responseURL, 1);
            analytics.SendData(searchEvent);

            // Send a response immediately, send the highlights as a delayed response
            res.status(200).end();
        }
    }
}