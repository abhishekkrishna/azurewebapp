var request = require("request");

module.exports = {
    postMessage: function(responseObj, channel) {
        "use strict";

        var TOKEN = "xoxb-206996222039-YNnMlG05TGvl1zIcZzrO7yHG";

        request({

            url: "https://slack.com/api/chat.postMessage?token=" + TOKEN + "&channel=" + channel + "&text=" + responseObj.text + "&attachments=" + responseObj.attachments,
            method: "POST"
        }, function (error, response, body) {
            if (!error) {
                console.log("Successfully sent chat message");
            }
            else {
                console.log("Error sending chat message");
                console.log(error);
            }
        });
    }
}