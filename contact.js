// Contact form
// Send a message to the feedback channel of summerfun

// ------------------------------------ IMPORTANT -------------------------------------------------------------------
/*

COMMENTS ARE ALWAYS INTENDED FOR THE LINE OF CODE THEY PRECEDE UNLESS EXPLICITLY MENTIONED OTHERWISE

*/
// -------------------------------------------------------------------------------------------------------------------

var sendMessage = require("./sendMessage.js");

var ChannelID = "C6C64KRPT";
var TeamID = "T5XU6JQLV";

// -------------------------------------------------------------------------------------------------------------------

module.exports = 
{
    send: function (req, res) 
    {
        "use strict";

        console.log("contact.js : 1 : " + req.body);

        var name = req.body.name;
        var email = req.body.email;
        var message = req.body.message;

        console.log("contact.js : 2 : Name: " + name);
        console.log("contact.js : 3 : Email: " + email);
        console.log("contact.js : 4 : Message: " + message);

        var Title = "*Message from " + name + " (`" + email + "`)*";

        var responseObj = 
        {
            "type": "message",
            "channel": ChannelID,
            "title": Title,
            "text": Title + "\n\n" + ">" + message,
            "mrkdwn_in":
            [
                "text"
            ]
        };
        sendMessage.send(responseObj, TeamID);

        res.redirect("/");
    }
};