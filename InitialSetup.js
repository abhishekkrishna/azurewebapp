// Initial Setup of tables when user sets up the app
// PARAMETERS: MediumName => medium username of the person

// Require these things
const jsdom = require("jsdom");
const {JSDOM} = jsdom;
var azure = require('azure-storage');
var WebSocket = require("ws");

// Require our functions
var sendMessage = require('./sendMessage.js');
var help = require("./help.js");
var scraper = require('./scraper');

module.exports = {
    Setup: function (SlackID, MediumName, responseObj, TeamID) {

        if (MediumName && SlackID) {
            var profileURL = "https://medium.com/@" + MediumName;
            JSDOM.fromURL(profileURL).then(function (dom) {
                // Extract the required things from the DOM
                // Username is valid, get the required fields
                var document = dom.window.document;
                var element = document.getElementsByClassName("followState");
                var MediumUserID = element[0].getAttribute("data-user-id");
                var elem = document.getElementsByClassName("hero-title");
                var DisplayName = elem[0].innerHTML;

                // Save the details to the User Table
                var tableSvc = azure.createTableService();

                var newUser = {
                    PartitionKey: {'_': 'User'},
                    RowKey: {'_': SlackID},
                    MediumUserID: {'_': MediumUserID},
                    DisplayName: {'_': DisplayName},
                    MediumName:{'_': MediumName}
                };

                // Insert into table
                tableSvc.insertOrReplaceEntity('MediumUsers', newUser, function (error, result, response) {
                    if (!error) {
                        // Entity inserted
                        responseObj.text = "_*Your username has been updated to " + MediumName + "!*_";
                        sendMessage.send(responseObj, TeamID, 0);
                        // Trigger the scraper for this user
                        scraper.Scraper(SlackID, MediumUserID, responseObj, MediumName, TeamID, 0);
                        // Send help message
                        //help.help(responseObj);

                        console.log("Successfully inserted " + MediumName + "!, User ID: " + MediumUserID);
                    } else {
                        // Error with table storage
                        responseObj.text = "Table storage error!";
                        sendMessage.send(responseObj, TeamID, 0);

                        console.log("Table storage error!");
                    }
                });
            }).catch(function (err) {
                // Username doesn't exist, we have an error
                responseObj.text = "That Medium username doesn't exist! Please enter a valid username.";
                sendMessage.send(responseObj, TeamID, 0);

                console.log(err);
            });
        } else {
            console.log("Function requires valid 'slackID' and 'MediumName' as a parameter!");
        }
    }
};