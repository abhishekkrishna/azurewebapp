// Respond to slash command with results

var request = require("request");
var sendObject = require("./sendObject.js");
var analytics = require("./analytics.js");
module.exports = {
    SlashResponse: function (SlackID,searchObj, responseURL, currentPageNumber, newPageNumber, Keyword) {

        var attachmentsObj = [];

        for (var i=0; i<searchObj.data.length; i++) {

            var highlightObj = searchObj.data[i];

            var startOffset = highlightObj.startOffset;
            var endOffset = highlightObj.endOffset;

            var quote = highlightObj.paragraph.substring(startOffset, endOffset);

            // Formatting the response object
            var obj = {
                //"pretext": "from *" + highlightObj.postName + "* by _" + highlightObj.postAuthor + "_",
                //"color": "#000000",
                "text": "from *" + highlightObj.postName + "* by _" + highlightObj.postAuthor + "_" + "\n" + "_*" + quote + "*_",
                "callback_id" : Keyword,
                "mrkdwn_in": [
                    "text",
                    "pretext"
                ],
                "color": "#01a96b",
                "actions": [
                    {
                        "name": highlightObj.highlightID,
                        "text": "Send as message",
                        "type": "button",
                        "value": "Send as message",
                        "style": "primary"
                    }
                ],
            };

            attachmentsObj.push(obj);
        }

        // Create actionButtons

        var returnText = "";

        if (Keyword === "~~~```~~~") {
            // There was no keyword entered
            returnText = "_Showing page *" + currentPageNumber + "* of your most recent highlights_"
        } else {
            // Text shown for a regular query
            returnText = "_Showing page *" + currentPageNumber + "* for the search term *" + Keyword + "*_";
        }

        var actionButtons = {
            "text": returnText,
            "callback_id" : Keyword,
            "mrkdwn_in": [
                "text"
            ],
            "actions": []
        };
        if (newPageNumber != 0) {
            actionButtons.actions.push({
                "name": "more",
                "text": "Show more highlights",
                "type": "button",
                "value": newPageNumber
            });
        }
        actionButtons.actions.push({
            "name": "cancel",
            "text": "Cancel",
            "type": "button",
            "value": "Cancel",
            "style": "danger"
        });
        attachmentsObj.push(actionButtons);

        // Create final response object
        var responseObj = {
            "response_type" : "ephemeral",
            "text" : "Here are your highlights:",
            "attachments": attachmentsObj
        }

        sendObject.SendObject(SlackID,responseURL, responseObj,0);
    }
}